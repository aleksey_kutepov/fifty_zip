from setuptools import find_packages
from setuptools import setup


major_version = '0.1'
minor_version = '0'
name = 'fifty_zip'

version = "%s.%s" % (major_version, minor_version)


if __name__ == "__main__":
    setup(name=name,
          version=version,
          description='Process 50 zip files',
          classifiers=[
              "Development Status :: 1 - Beta",
              "Programming Language :: Python",
          ],
          author='Aleksey Kutepov',
          author_email='kutepoff@gmail.com',
          packages=find_packages(),
          zip_safe=False,
          entry_points={
              'console_scripts': [
                  '50zip_create = fifty_zip.50zip_create:main',
                  '50zip_single = fifty_zip.50zip_sync:main',
                  '50zip_process = fifty_zip.50zip_process:main',
              ],
          },
    )