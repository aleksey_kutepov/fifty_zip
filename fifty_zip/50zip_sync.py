"""Script for processing those zip files.

Two comma separated files will be created in current directory:

id_level.csv -- one-to-one mapping between uniq ID and LEVEL
id_objects.csv -- one-to-many mapping between uniq ID and each object in 
                  objects section from the source data file.
                  
"""
import glob
import re
import timeit

from fifty_zip.settings import *
from fifty_zip.mappers import get_id_level_objects

ID_REGEX = re.compile("<var name='id' value='([a-z0-9-]+)'/>")
LEVEL_REGEX = re.compile("<var name='level' value='([0-9]+)'/>")
OBJECT_NAME_REGEX = re.compile("<object name='([A-Z0-9]+)'/>")


def main():
    with open(ID_LEVEL_OUT, 'w') as id_level,\
            open(ID_OBJECTS_OUT, 'w') as id_objects:
        for file_name in glob.glob(ZIP_FILES_DIR_NAME + '/*.zip'):
            uid, level, objs = get_id_level_objects(file_name)
            id_level.write(','.join([uid, level]) + '\n')
            id_objects.writelines([','.join([uid, obj]) + '\n' for obj in objs])


if __name__ == '__main__':
    print(f'Execution time {timeit.timeit(main, number=1)}')
