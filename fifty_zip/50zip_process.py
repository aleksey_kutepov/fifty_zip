"""Script for processing those zip files utilizing as much CPU as possible.

To utilize all CPU cores thread pool is used. Number of threads is a subject 
to change from host to host. The value is purely empirical knowledge.

Two comma separated files will be created in current directory:

id_level.csv -- one-to-one mapping between uniq ID and LEVEL
id_objects.csv -- one-to-many mapping between uniq ID and each object in 
                  objects section from the source data file.
                  
"""
import glob
import timeit

from multiprocessing.dummy import Pool

from fifty_zip.settings import *
from fifty_zip.mappers import get_id_level_objects


def main():
    pool = Pool(THREADS_NUM)  # the number vary from host to host
    result = pool.map(get_id_level_objects, glob.glob(ZIP_FILES_DIR_NAME + '/*.zip'))
    with open(ID_LEVEL_OUT, 'w') as id_level, open(ID_OBJECTS_OUT, 'w') as id_objects:
        for file_name, uid, level, objs in result:
            if ID_LEVEL_OUT == file_name:
                id_level.write(','.join([uid, level]) + '\n')
            else:
                id_objects.writelines([','.join([uid, obj]) + '\n' for obj in objs])


if __name__ == '__main__':
    print(f'Execution time {timeit.timeit(main, number=1)}')
