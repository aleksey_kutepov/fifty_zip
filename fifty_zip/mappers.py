"""This file contains shared code"""
import re
import zipfile

import io

from fifty_zip.settings import *

ID_REGEX = re.compile("<var name='id' value='([a-z0-9-]+)'/>")
LEVEL_REGEX = re.compile("<var name='level' value='([0-9]+)'/>")
OBJECT_NAME_REGEX = re.compile("<object name='([A-Z0-9]+)'/>")


def get_id_level_objects(file_name):
    """Extract all necessary data from a zip file.

    This function implemented with several assumptions. 

    First is that we have small files as it described in the task. So we can
    read a whole file into memory safely.

    Second. The structure of the file is fixed. I.e. we can find `id` on the
    second line of the file. That significantly speeds up extraction. Otherwise
    it would require full fledged XML parser (or custom) creating an AST parsing
    each token.
    """
    with zipfile.ZipFile(file_name) as zf, \
            zf.open(ZIP_FILES_INT_NAME) as int_file:
        # Read all lines into memory
        lines = io.TextIOWrapper(int_file).readlines()
        # Since we now the structure of input file we can optimize reads
        uid = ID_REGEX.match(lines[1]).group(1)  # exact line with ID
        level = LEVEL_REGEX.match(lines[2]).group(1)  # exact line with LEVEL
        # Known lines which contain information about object's name
        objs = [OBJECT_NAME_REGEX.match(line).group(1) for line in lines[4:-2]]
        return uid, level, objs


