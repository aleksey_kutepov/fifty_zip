"""Script for creating 50 zip files with predefined structure."""
import os
import uuid
import random
import zipfile
import timeit
import errno

from fifty_zip.settings import *

random.seed()


def get_random_string(size=6):
    return ''.join(random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789')
                   for _ in range(size))


def get_objects():
    size = random.randint(1, 10)
    return '\n'.join(f"<object name='{get_random_string()}'/>"
                     for _ in range(size))


def get_file_content():
    """Template for the predefined XML alike file."""
    return f'''<root>
<var name='id' value='{uuid.uuid4()}'/>
<var name='level' value='{random.randint(1,100)}'/>
<objects>
{get_objects()}
</objects>
</root>
'''


def save_zip_file(file_name, content):
    with zipfile.ZipFile(file_name, 'w') as zf:
        zf.writestr('content.txt', content)


def make_dir(name):
    try:
        os.makedirs(name)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise


def main():
    make_dir(ZIP_FILES_DIR_NAME)
    for i in range(ZIP_FILES_AMOUNT):
        save_zip_file(f'{ZIP_FILES_DIR_NAME}/{i}.zip', get_file_content())


if __name__ == '__main__':
    print(f'Execution time {timeit.timeit(main, number=1)}')
